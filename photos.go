package vk

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
)

type photosGetResponse struct {
	Response struct {
		Count int     `json:"count"`
		Items []Photo `json:"items"`
	} `json:"response"`
}

func Photos_get(p Params) (photos []Photo, err error) {
	resp, err := Request("photos.get", p)
	if err != nil {
		return
	}
	err = resp.GetErrors()
	if err != nil {
		if vkerr, ok := err.(*VkError); ok {
			code, message := vkerr.ErrorCode, vkerr.ErrorMsg
			err = errors.New(fmt.Sprintf("Ошибка в ответе vk #%d: %s", code, message))
		}
		return
	}
	var r photosGetResponse
	err = resp.Unmarshal(&r)
	if err != nil {
		return
	}
	photos = r.Response.Items
	return
}

type photosGetOwnerCoverPhotoUploadServerResponse struct {
	Response struct {
		UploadUrl string `json:"upload_url"`
	} `json:"response"`
}

// https://vk.com/dev/photos.getOwnerCoverPhotoUploadServer
func Photos_getOwnerCoverPhotoUploadServer(p Params) (uploadUrl string, err error) {
	resp, err := Request("photos.getOwnerCoverPhotoUploadServer", p)
	if err != nil {
		return "", err
	}
	err = resp.GetErrors()
	if err != nil {
		if vkerr, ok := err.(*VkError); ok {
			code, message := vkerr.ErrorCode, vkerr.ErrorMsg
			err = errors.New(fmt.Sprintf("Ошибка в ответе vk #%d: %s", code, message))
		}
		return "", err
	}
	var r photosGetOwnerCoverPhotoUploadServerResponse
	err = resp.Unmarshal(&r)
	if err != nil {
		return "", err
	}
	return r.Response.UploadUrl, nil
}

//https://vk.com/dev/upload_files_2?f=11.%2B%D0%97%D0%B0%D0%B3%D1%80%D1%83%D0%B7%D0%BA%D0%B0%2B%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B8%2B%D1%81%D0%BE%D0%BE%D0%B1%D1%89%D0%B5%D1%81%D1%82%D0%B2%D0%B0
func Photos_UploadCoverImage(url string, photo io.Reader) (*CoverPhoto, error) {
	// создать объект multipart формы
	var b bytes.Buffer
	form := multipart.NewWriter(&b)
	photoField, err := form.CreateFormFile("photo", "photo")
	if err != nil {
		return nil, err
	}
	_, err = io.Copy(photoField, photo)
	if err != nil {
		return nil, err
	}
	form.Close()

	// отослать http запрос
	req, err := http.NewRequest("POST", url, &b)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", form.FormDataContentType())
	client := &http.Client{}

	//fmt.Println("req:", req)

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		err = fmt.Errorf("photos.go: bad status: %s", res.Status)
		return nil, err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	res.Body.Close()
	var cover CoverPhoto
	err = cover.UnmarshalJSON(body)
	return &cover, err
}

type photosSaveOwnerCoverPhotoResponse struct {
	Response struct {
		Images []BaseImage `json:"images"`
	} `json:"response"`
}

func Photos_saveOwnerCoverPhoto(p Params) (images []BaseImage, err error) {
	resp, err := Request("photos.saveOwnerCoverPhoto", p)
	if err != nil {
		return nil, err
	}
	err = resp.GetErrors()
	if err != nil {
		if vkerr, ok := err.(*VkError); ok {
			code, message := vkerr.ErrorCode, vkerr.ErrorMsg
			err = errors.New(fmt.Sprintf("Ошибка в ответе vk #%d: %s", code, message))
		}
		return nil, err
	}
	var r photosSaveOwnerCoverPhotoResponse
	err = resp.Unmarshal(&r)
	if err != nil {
		return nil, err
	}
	return r.Response.Images, nil
}
