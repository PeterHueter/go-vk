package vk

import (
	"errors"
	"fmt"
)

type GroupsGetByIdResponse struct {
	Response []Group `json:"response"`
}

// информация о сообщест(ве/вах)
// https://vk.com/dev/groups.getById
func Groups_getById(p Params) (groups []Group, err error) {
	resp, err := Request("groups.getById", p)
	if err != nil {
		return
	}
	err = resp.GetErrors()
	if err != nil {
		if vkerr, ok := err.(*VkError); ok {
			code, message := vkerr.ErrorCode, vkerr.ErrorMsg
			err = errors.New(fmt.Sprintf("Ошибка в ответе vk #%d: %s", code, message))
		}
		return
	}

	var r GroupsGetByIdResponse
	err = resp.Unmarshal(&r)
	if err != nil {
		return
	}
	groups = r.Response
	return
}

type GroupsInviteResponse struct {
	Result int `json:"response"` // 1
}

func Groups_invite(p Params) (result int, err error) {
	resp, err := Request("groups.invite", p)
	if err != nil {
		return
	}
	err = resp.GetErrors()
	if err != nil {
		if vkerr, ok := err.(*VkError); ok {
			code, message := vkerr.ErrorCode, vkerr.ErrorMsg
			err = errors.New(fmt.Sprintf("Ошибка в ответе vk #%d: %s", code, message))
		}
		return
	}

	var r GroupsInviteResponse
	err = resp.Unmarshal(&r)
	if err != nil {
		return
	}
	result = r.Result
	return
}
