package vk

import (
	"errors"
	"fmt"
)

type MessagesSendResponse struct {
	Response int `json:"response"`
}

// Отправить сообщение пользователю, требуется access token
func Messages_send(p Params) (message_id int, err error) {
	resp, err := Request("messages.send", p)
	if err != nil {
		return
	}
	err = resp.GetErrors()
	if err != nil {
		if vkerr, ok := err.(*VkError); ok {
			code, message := vkerr.ErrorCode, vkerr.ErrorMsg
			err = errors.New(fmt.Sprintf("Ошибка в ответе vk #%d: %s", code, message))
		}
		return
	}

	var r MessagesSendResponse
	err = resp.Unmarshal(&r)
	if err != nil {
		return
	}
	message_id = r.Response
	return
}

type MessagesGetRespnse struct {
	Response struct {
		Count int       `json:"count"`
		Items []Message `json:"items"`
	} `json:"response"`
}

func Messages_get(p Params) (messages []Message, err error) {
	resp, err := Request("messages.get", p)
	if err != nil {
		return
	}
	err = resp.GetErrors()
	if err != nil {
		if vkerr, ok := err.(*VkError); ok {
			code, message := vkerr.ErrorCode, vkerr.ErrorMsg
			err = fmt.Errorf("messages.get: код %d: %s", code, message)
		}
		return
	}

	var r MessagesGetRespnse
	err = resp.Unmarshal(&r)
	if err != nil {
		return
	}
	messages = r.Response.Items
	return
}
