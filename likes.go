package vk

import (
	"errors"
	"fmt"
)

type likesGetListResponse struct {
	Response struct {
		Count int   `json:"count"`
		Items []int `json:"items"`
	} `json:"response"`
}

func Likes_getList(p Params) (likes []int, err error) {
	resp, err := Request("likes.getList", p)
	if err != nil {
		return
	}
	err = resp.GetErrors()
	if err != nil {
		if vkerr, ok := err.(*VkError); ok {
			code, message := vkerr.ErrorCode, vkerr.ErrorMsg
			err = fmt.Errorf("likes.getList: код %d: %s", code, message)
		}
		return
	}
	var r likesGetListResponse
	err = resp.Unmarshal(&r)
	if err != nil {
		return
	}
	likes = r.Response.Items
	return
}

func Likes_add(p Params) (likes int, err error) {
	resp, err := Request("likes.add", p)
	if err != nil {
		return
	}
	err = resp.GetErrors()
	if err != nil {
		if vkerr, ok := err.(*VkError); ok {
			code, message := vkerr.ErrorCode, vkerr.ErrorMsg
			err = errors.New(fmt.Sprintf("Ошибка в ответе vk #%d: %s", code, message))
		}
		return
	}
	var r likesAddResponse
	err = resp.Unmarshal(&r)
	if err != nil {
		return
	}
	likes = r.Response.Likes
	return
}

type likesAddResponse struct {
	Response struct {
		Likes int `json:"likes"`
	} `json:"response"`
}
