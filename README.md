# go-vk

Набор функций для работы с API vk.

## Пример использования

```
package main

import (
	"fmt"

	vk "bitbucket.org/PeterHueter/go-vk"
)

func main() {
	p := vk.Params{
		"group_id": "1",
	}
	topics, err := vk.Board_getTopics(p)
	if err != nil {
		fmt.Println(err)
		return
	}
	for id, item := range topics {
		fmt.Printf("%d: %s\n", id, item.Title)
	}
}
```
