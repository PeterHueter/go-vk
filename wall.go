package vk

import (
	"fmt"
)

type WallGetResponse struct {
	Response struct {
		Count int    `json:"count"`
		Items []Post `json:"items"`
	} `json:"response"`
}

func Wall_get(p Params) ([]Post, error) {
	resp, err := Request("wall.get", p)
	if err != nil {
		return nil, err
	}
	err = resp.GetErrors()
	if err != nil {
		if vkerr, ok := err.(*VkError); ok {
			code, message := vkerr.ErrorCode, vkerr.ErrorMsg
			err = fmt.Errorf("wall.get: код %d: %s", code, message)
		}
		return nil, err
	}
	var r WallGetResponse
	err = resp.Unmarshal(&r)
	if err != nil {
		return nil, err
	}

	return r.Response.Items, nil
}

type WallGetCommentsResponse struct {
	Response struct {
		Count    int       `json:"count"`
		Items    []Comment `json:"items"`
		Profiles []User    `json:"profiles"`
		Groups   []Group   `json:"groups"`
	} `json:"response"`
}

func Wall_getComments(p Params) (comments []Comment, users []User, groups []Group, err error) {
	resp, err := Request("wall.getComments", p)
	if err != nil {
		return
	}
	err = resp.GetErrors()
	if err != nil {
		if vkerr, ok := err.(*VkError); ok {
			code, message := vkerr.ErrorCode, vkerr.ErrorMsg
			err = fmt.Errorf("wall.getcomments: код %d: %s", code, message)
		}
		return
	}
	var r WallGetCommentsResponse
	err = resp.Unmarshal(&r)
	if err != nil {
		return
	}
	comments = r.Response.Items
	users = r.Response.Profiles
	groups = r.Response.Groups
	return
}
