//go:generate easyjson -all $GOFILE
package vk

// Объект ошибки
type VkError struct {
	ErrorCode     int    `json:"error_code"`
	ErrorMsg      string `json:"error_msg"`
	RequestParams []struct {
		Key   string `json:"key"`
		Value string `json:"value"`
	} `json:"request_params"`
	CaptchaSid string `json:"captcha_sid,omitempty"`
	CaptchaImg string `json:"captcha_img,omitempty"`
}

func (e VkError) Error() string         { return e.ErrorMsg }
func (e VkError) Json() (string, error) { b, err := e.MarshalJSON(); return string(b), err }

// Объект пользователя возвращаемый некоторыми методами
// https://vk.com/dev/fields
type User struct {
	ID         int    `json:"id"`
	FirstName  string `json:"first_name"`
	LastName   string `json:"last_name"`
	Sex        int    `json:"sex"`
	ScreenName string `json:"screen_name"`
	// ... many more
}

// Запись на стене пользователя или сообщества
// https://vk.com/dev/post
type Post struct {
	Id       int    `json:"id"`
	FromID   int    `json:"from_id"`
	Owner_id int    `json:"owner_id"`
	Date     int64  `json:"date"`
	PostType string `json:"post_type"`
	Text     string `json:"text"`
	Comments struct {
		Count int `json:"count"`
	} `json:"comments"`
	Likes struct {
		Count int `json:"count"`
	} `json:"likes"`
	Reposts struct {
		Count int `json:"count"`
	} `json:"reposts"`
}

// Объект, описывающий комментарий из обсуждения в сообществе
// https://vk.com/dev/comment_board
type Comment_board struct {
	Id      int    `json:"id"`
	From_id int    `json:"from_id"`
	Date    int64  `json:"date"`
	Text    string `json:"text"`
	Likes   struct {
		Count int `json:"count"`
	} `json:"likes"`
}

// Объект group, описывающий сообщество
// https://vk.com/dev/fields_groups
type Group struct {
	ID         int    `json:"id"`
	Name       string `json:"name"`
	ScreenName string `json:"screen_name"`
	IsClosed   int    `json:"is_closed"`
	Type       string `json:"type"`
}

// Объект возвращаемый board.getTopics
type Topic struct {
	ID        int    `json:"id"`
	Title     string `json:"title"`
	Created   int    `json:"created"`
	CreatedBy int    `json:"created_by"`
	Updated   int    `json:"updated"`
	UpdatedBy int    `json:"updated_by"`
	IsClosed  int    `json:"is_closed"`
	IsFixed   int    `json:"is_fixed"`
	Comments  uint   `json:"comments"`
}

// Объект описывающий личное сообщение
type Message struct {
	ID        int    `json:"id"`
	Date      int64  `json:"date"`
	Out       int    `json:"out"`
	UserID    int    `json:"user_id"`
	ReadState int    `json:"read_state"`
	Title     string `json:"title"`
	Body      string `json:"body"`
}

// Комментарий к посту
type Comment struct {
	ID     int    `json:"id"`
	FromID int    `json:"from_id"`
	Date   int64  `json:"date"`
	Text   string `json:"text"`
	Likes  struct {
		Count int `json:"count"`
	} `json:"likes"`
	ReplyToUser    int `json:"reply_to_user,omitempty"`
	ReplyToComment int `json:"reply_to_comment,omitempty"`

	//Attachments []struct {} `json:"attachments,omitempty"`
}

// Фотография в альбоме
type Photo struct {
	ID      int `json:"id"`
	AlbumID int `json:"album_id"`
	OwnerID int `json:"owner_id"`
	// если photo_sizes=1
	Sizes []struct {
		Src    string `json:"src"`
		Width  int    `json:"width"`
		Height int    `json:"height"`
		Type   string `json:"type"`
	} `json:"sizes"`
	Text   string  `json:"text"`
	Date   int     `json:"date"`
	PostID int     `json:"post_id"`
	Lat    float64 `json:"lat,omitempty"`
	Long   float64 `json:"long,omitempty"`
}

// Ответ на запрос utils.resolveScreenName
// type может быть user, group, application, page
type ResolveInfo struct {
	Type     string `json:"type"`
	ObjectID int    `json:"object_id"`
}

// Объект возвращаемый после загрузки фотографии для смены обложки сообщества
type CoverPhoto struct {
	Hash  string `json:"hash"`
	Photo string `json:"photo"`
}

type BaseImage struct {
	Url    string `json:"url"`
	Width  int    `json:"width"`
	Height int    `json:"height"`
}
