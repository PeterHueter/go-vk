package vk

import (
	"errors"
	"net/url"
	"regexp"
	"strconv"
	"strings"
)

const (
	// TypeTopic Ссылка указывает на обсуждеие
	TypeTopic = "topic"

	// TypeBoard ссылка указывает на список обсуждений
	TypeBoard = "board"

	// TypeWall ссылка на стену
	TypeWall = "wall"
)

// VkUrl Объект ссылки в vk
type VkUrl struct {
	Type       string // тип ссылки topic, wall, board оставлен для совместимости
	VkType     string // тип возвращаемый методом utils.resolveScreenName
	Id         int    // Id профиля/группы
	Tid        int    // Id обсуждения
	ScreenName string // Коротка ссылка
	Reply      int    // Id комментария в посте на стене
	Post       int    // Id комментария в обсуждении
	Offset     int    // Сдвиг относительно начала
}

// NewUrl Создаёт новый объект ссылки и заполняет известую информацию о ней
func NewUrl(link string) (vkurl *VkUrl, err error) {
	u, err := url.Parse(link)
	if err != nil {
		return nil, errors.New("NewUrl: " + err.Error())
	}

	if u.Host != "vk.com" {
		return nil, errors.New(link + " не является адресом vk")
	}

	str := u.Path
	str = str[1:]

	vkurl = &VkUrl{}

	// определить тип ссылки
	if strings.Contains(str, "topic-") { // ссылка на обсуждение
		vkurl.Type = TypeTopic

		// определить gid и tid
		re := regexp.MustCompile(`(\d+)`)
		numbers := re.FindAllString(link, -1)
		if len(numbers) != 2 {
			err = errors.New(link + " не содержит gid и tid")
			return
		}
		vkurl.Id, _ = strconv.Atoi(numbers[0])
		vkurl.Id = -vkurl.Id // id груупы отрицательное число
		vkurl.Tid, _ = strconv.Atoi(numbers[1])
		return
	} else if strings.Contains(str, "board") { // ссылка на список обсуждений
		vkurl.Type = TypeBoard
		// определить gid
		re := regexp.MustCompile(`(\d+)`)
		number := re.FindAllString(link, -1)
		if len(number) != 1 {
			err = errors.New(link + " не содержит gid")
			return
		}
		vkurl.Id, _ = strconv.Atoi(number[0])
		vkurl.Id = -vkurl.Id
		return
	} else { // ссылка на стену пользователя/группы или ошибка
		vkurl.ScreenName = str
		params := Params{
			"screen_name": str,
		}

		info, err := Utils_resolveScreenName(params)
		if err != nil {
			return nil, err
		}

		vkurl.VkType = info.Type

		if info.Type == "user" {
			vkurl.Id = info.ObjectID
			vkurl.Type = TypeWall
		} else if info.Type == "group" {
			vkurl.Id = -info.ObjectID
			vkurl.Type = TypeWall
		} else {
			err = errors.New("Тип ссылки не поддерживается")
		}
	}
	return
}
