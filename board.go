package vk

import "fmt"
import "errors"

type boardCommentsResponse struct {
	Response struct {
		Count int64           `json:"count,omitempty"`
		Items []Comment_board `json:"items,omitempty"`
		// extended=1
		Profiles []User  `json:"profiles"`
		Groups   []Group `json:"groups"`
	}
}

// Получить список комментариев в обсуждении
func Board_getComments(p Params) (comments []Comment_board, profiles []User, groups []Group, err error) {
	resp, err := Request("board.getComments", p)

	if err != nil {
		return
	}
	err = resp.GetErrors()
	if err != nil {
		return
	}

	var r boardCommentsResponse
	err = resp.Unmarshal(&r)
	if err != nil {
		return
	}
	comments = r.Response.Items
	profiles = r.Response.Profiles
	groups = r.Response.Groups
	return
}

type BoardGetTopicsResponse struct {
	Response struct {
		Count        int     `json:"count"`
		Items        []Topic `json:"items"`
		DefaultOrder float64 `json:"default_order"`
		CanAddTopics int     `json:"can_add_topics"`
	} `json:"response"`
}

func Board_getTopics(p Params) (topics []Topic, err error) {
	resp, err := Request("board.getTopics", p)
	if err != nil {
		return
	}
	err = resp.GetErrors()
	if err != nil {
		if vkerr, ok := err.(*VkError); ok {
			code, message := vkerr.ErrorCode, vkerr.ErrorMsg
			err = errors.New(fmt.Sprintf("Ошибка в ответе vk #%d: %s", code, message))
		}
		return
	}

	var r BoardGetTopicsResponse
	err = resp.Unmarshal(&r)
	if err != nil {
		return
	}
	topics = r.Response.Items
	return
}
