package vk

import (
	"errors"
	"fmt"
)

type adsimportTargetContactsResponse struct {
	Response int `json:"response"`
}

func Ads_importTargetContacts(p Params) (imported int, err error) {
	resp, err := Request("ads.importTargetContacts", p)
	if err != nil {
		return
	}
	err = resp.GetErrors()
	if err != nil {
		if vkerr, ok := err.(*VkError); ok {
			code, message := vkerr.ErrorCode, vkerr.ErrorMsg
			err = errors.New(fmt.Sprintf("Ошибка в ответе vk #%d: %s", code, message))
		}
		return
	}
	var r adsimportTargetContactsResponse
	err = resp.Unmarshal(&r)
	if err != nil {
		return
	}
	imported = r.Response
	return
}
