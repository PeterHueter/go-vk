package vk

import (
	"errors"
	"fmt"
)

// ErrScreenNameNotFound если не получилось найти screen name
var ErrScreenNameNotFound = errors.New("screen name не существует")

type UtilsResolveScreenNameResponse struct {
	Info ResolveInfo `json:"response"`
}

func Utils_resolveScreenName(p Params) (*ResolveInfo, error) {
	resp, err := Request("utils.resolveScreenName", p)
	if err != nil {
		return nil, err
	}

	if string(resp) == `{"response":[]}` {
		return nil, ErrScreenNameNotFound
	}

	err = resp.GetErrors()
	if err != nil {
		if vkerr, ok := err.(*VkError); ok {
			code, message := vkerr.ErrorCode, vkerr.ErrorMsg
			err = fmt.Errorf("utils.resolveScreenName: код %d: %s", code, message)
		}
		return nil, err
	}
	var r UtilsResolveScreenNameResponse
	err = resp.Unmarshal(&r)
	if err != nil {
		return nil, err
	}

	return &r.Info, err
}
