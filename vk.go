package vk

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

const (
	API_METHOD_URL      = "https://api.vk.com/method/"
	API_DEFAULT_VERSION = "5.67"
)

type Response []byte

func (r *Response) Unmarshal(v interface{}) (err error) {
	err = json.Unmarshal(*r, v)
	return
}

// Request отправляет запрос к API vk
func Request(method string, params Params) (Response, error) {
	// восстановление после паники
	// defer func() {
	// 	if r := recover(); r != nil {
	// 		err = errors.New(r.(string))
	// 	}
	// }()

	u, err := url.Parse(API_METHOD_URL + method)
	if err != nil {
		return nil, fmt.Errorf("vk request: %s", err)
	}
	q := u.Query()
	q.Set("v", API_DEFAULT_VERSION)
	for k, v := range params {
		q.Set(k, v)
	}
	u.RawQuery = q.Encode()

	timeout := time.Duration(10 * time.Second) // 10 сек таймаут запроса
	client := http.Client{
		Timeout: timeout,
	}

	resp, err := client.Get(u.String())
	if err != nil {
		return nil, fmt.Errorf("send vk request: %s", err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()

	return body, err
}
