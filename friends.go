package vk

import (
	"errors"
	"fmt"
)

type FriendRequestResult int

const (
	FRIEND_REQUEST_SUCCESS       FriendRequestResult = 1
	FRIEND_REQUEST_APPROVED      FriendRequestResult = 2
	FRIEND_REQUEST_ALAREADY_SENT FriendRequestResult = 4
)

type FriendsAddResponse struct {
	Result FriendRequestResult `json:"response"`
}

// запрос на добавление в друзья
func Friends_add(p Params) (result FriendRequestResult, err error) {
	resp, err := Request("friends.add", p)
	if err != nil {
		return
	}
	err = resp.GetErrors()
	if err != nil {
		if vkerr, ok := err.(*VkError); ok {
			code, message := vkerr.ErrorCode, vkerr.ErrorMsg
			err = errors.New(fmt.Sprintf("Ошибка в ответе vk #%d: %s", code, message))
		}
		return
	}

	var r FriendsAddResponse
	err = resp.Unmarshal(&r)
	if err != nil {
		return
	}
	result = r.Result
	return
}
