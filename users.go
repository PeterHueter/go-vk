package vk

import (
	"fmt"
)

type UsersGetResponse struct {
	Response []User `json:"response"`
}

func Users_get(p Params) (users []User, err error) {
	resp, err := Request("users.get", p)
	if err != nil {
		return
	}
	err = resp.GetErrors()
	if err != nil {
		if vkerr, ok := err.(*VkError); ok {
			code, message := vkerr.ErrorCode, vkerr.ErrorMsg
			err = fmt.Errorf("users.get", code, message)
		}
		return
	}
	var r UsersGetResponse
	err = resp.Unmarshal(&r)
	if err != nil {
		return
	}
	users = r.Response
	return
}
