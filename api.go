package vk

import (
	"encoding/json"
)

type Params map[string]string

// объект ошибки
type ErrorsResponse struct {
	Error VkError
}

// Проверяет ответ на наличие ошибок
func (r Response) GetErrors() error {
	var e = ErrorsResponse{Error: VkError{ErrorCode: 0}}
	err := json.Unmarshal(r, &e)
	if err != nil {
		return err
	}

	// случилась ошибка
	if e.Error.ErrorCode != 0 {
		return &e.Error
	}
	return nil
}
